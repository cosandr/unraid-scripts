# Install

1. Install `Community Applications`
`https://raw.githubusercontent.com/Squidly271/community.applications/master/plugins/community.applications.plg`

2. Install `CA User Scripts`

3. Copy script folder to `config/plugins/user.scripts/scripts` on boot drive

# Special variables

Put directly under shebang line.

* `description` This is the description of the script - ie: what will show up on the UI for the plugin

* `foregroundOnly` Setting this to be true disallows background running (and scheduling) of the script

* `backgroundOnly` Setting this to be true disallows foreground running of the script

* `arrayStarted` Setting this to be true will only run the script (foreground or background or scheduled) if the array is up and running

* `name` this is the name of the script.  Without this variable, the GUI will display the folder's name

* `argumentDescription` if present this will bring up a pop up asking the user for the argument list for the script.  Note that currently arguments do not accept spaces contained within one argument (ie: quoting and escaping spaces does NOT work)

* `argumentDefault` this is the default arguments for the above

* `clearLog` Set to be true to have the log deleted prior to execution of the script (ie: logs are only for the last execution of the script)

* `noParity` Set to be try if the script is not allowed to run when a parity check / rebuild is in progress
