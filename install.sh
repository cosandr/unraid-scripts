#!/bin/bash


if [[ $EUID -ne 0 ]]; then
    echo "Must run as root"
    exit 1
fi

CONFIG_INSTALL_DIR="/boot/config"
SCRIPT_INSTALL_DIR="${CONFIG_INSTALL_DIR}/plugins/user.scripts/scripts"
if [[ ! -d "$SCRIPT_INSTALL_DIR" ]]; then
    echo "$SCRIPT_INSTALL_DIR not found, is CA User Scripts installed?"
fi

cp -rfv config/* "$CONFIG_INSTALL_DIR/"
